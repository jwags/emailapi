using AutoMapper;
using EmailApi.ContextConfiguration;
using EmailApi.Business.BusinessLogic;
using EmailApi.Business.Models;
using EmailApi.Business.Services;
using EmailApi.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;

namespace EmailApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailsController : ControllerBase
    {
        private readonly IMapper _Mapper;
        private readonly IEmailRepository _EmailRepository;
        private readonly IEmailBusinessLogic _EmailBusinessLogic;

        public EmailsController(EmailContext context, IEmailRepository emailRepository, IMapper mapper, IEmailBusinessLogic emailBusinessLogic)
        {
            _EmailBusinessLogic = emailBusinessLogic;
            _EmailRepository = emailRepository;
            _Mapper = mapper;
        }

        // GET: api/Emails/v1
        [HttpGet("v1")]
        //public async Task<ActionResult<IEnumerable<Email>>> GetEmails()
        public ActionResult<IEnumerable<Email>> GetEmails()
        {
            // no db setup yet
            return Ok();//await _EmailRepository.GetEmails());
        }

        // GET: api/Emails/v1
        [HttpPost("SendEmail/v1")]
        [EnableCors("ApiCorsPolicy-AuthorizedOrigins")]
        //public async Task<ActionResult<IEnumerable<Email>>> GetEmails()
        public async Task<ActionResult<Email>> SendEmail(EmailCommandModel email)
        {
            var result = await _EmailBusinessLogic.SendEmail(email);

            if(result.IsFailure())
            {
                return BadRequest(result.ErrorMessage);
            }

            return Ok();
        }

        // GET: api/Emails/5
        // [HttpGet("v1/{id}")]
        // public async Task<ActionResult<Email>> GetEmail(int id)
        // {
        //     var project = await _context.Emails.FindAsync(id);

        //     if (project == null)
        //     {
        //         return NotFound();
        //     }

        //     return project;
        // }

        // PUT: api/Emails/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        // [HttpPut("v1/{id}")]
        // public async Task<IActionResult> PutEmail(int id, Email project)
        // {
        //     if (id != project.Id)
        //     {
        //         return BadRequest();
        //     }

        //     _context.Entry(project).State = EntityState.Modified;

        //     try
        //     {
        //         await _context.SaveChangesAsync();
        //     }
        //     catch (DbUpdateConcurrencyException)
        //     {
        //         if (!EmailExists(id))
        //         {
        //             return NotFound();
        //         }
        //         else
        //         {
        //             throw;
        //         }
        //     }

        //     return NoContent();
        // }

        // POST: api/Emails
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        // [HttpPost("v1")]
        // public async Task<ActionResult<Email>> PostEmail(Email project)
        // {
        //     _context.Emails.Add(project);
        //     await _context.SaveChangesAsync();

        //     return CreatedAtAction(nameof(GetEmail), new { id = project.Id }, project);
        // }

        // DELETE: api/Emails/5
        // [HttpDelete("v1/{id}")]
        // public async Task<ActionResult<Email>> DeleteEmail(int id)
        // {
        //     var project = await _context.Emails.FindAsync(id);
        //     if (project == null)
        //     {
        //         return NotFound();
        //     }

        //     _context.Emails.Remove(project);
        //     await _context.SaveChangesAsync();

        //     return project;
        // }

        // private bool EmailExists(int id)
        // {
        //     return _context.Emails.Any(e => e.Id == id);
        // }
    }
}

using EmailApi.Business.BusinessLogic;
using EmailApi.ContextConfiguration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using EmailApi.Business.Services;
using EmailApi.Mapping;
using Microsoft.AspNetCore.HttpOverrides;

namespace EmailApi.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options => 
            {
                options.AddPolicy("ApiCorsPolicy-AuthorizedOrigins", 
                    builder => builder
                        .WithOrigins("http://localhost:4200","https://www.Dev.JordansPortfolio.com","https://Dev.JordansPortfolio.com","https://www.JordansPortfolio.com","https://JordansPortfolio.com")
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader());
            });

            // services.AddCors(options => options.AddPolicy("ApiCorsPolicy-AnyCall", builder =>
            // {
            //     builder.WithOrigins("http://localhost:4200")
            //         .AllowAnyMethod()
            //         .AllowAnyHeader();
            // }));

            // register the DbContext on the container
            services.AddDbContext<EmailContext>();

            // register the repository
            services.AddScoped<IEmailRepository, EmailRepository>();

            // register the business logic
            services.AddScoped<IEmailBusinessLogic, EmailBusinessLogic>();

            // register automapper profiles
            AutoMapperProfile.Build(services);

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors("ApiCorsPolicy-AuthorizedOrigins");

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

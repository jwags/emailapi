using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using EmailApi.Business.Models;

namespace EmailApi.Business.Helpers
{
    public enum DataAnnotationsValidationResultCode
    {
        None = 0,
        Okay = 1,
        Invalid = -1
    }

    public static class DataAnnotationsHelper
    {
        public static IBusinessResult<DataAnnotationsValidationResultCode, TModel> ValidateModel<TModel>(TModel model)
            where TModel : class
        {
            var validationResults = new List<ValidationResult>();
            var context = new ValidationContext(model, null, null);
            Validator.TryValidateObject(model, context, validationResults, true);

            if (validationResults.Count > 0)
            {
                var errorMessages = validationResults.Select(x => new KeyValuePair<string, string>("DataAnnotationValidationError", x.ErrorMessage)).ToList();
                return new BusinessResult<DataAnnotationsValidationResultCode, TModel>(false, DataAnnotationsValidationResultCode.Invalid, string.Empty, model);
            }

            return new BusinessResult<DataAnnotationsValidationResultCode, TModel>(true, DataAnnotationsValidationResultCode.Okay, string.Empty, model);
        }
    }
}

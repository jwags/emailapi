using Microsoft.EntityFrameworkCore;
using EmailApi.ContextConfiguration;
using EmailApi.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailApi.Business.Services
{
    public interface IEmailRepository
    {
        Task<IEnumerable<Email>> GetEmails();
    }

    public class EmailRepository : IEmailRepository
    {
        public EmailRepository(EmailContext context)
        {
            _Context = context;
        }

        private readonly EmailContext _Context;

        public async Task<IEnumerable<Email>> GetEmails()
        {
            return await _Context.Emails
                           .Where(project => project.IsVoid == false)
                           .ToListAsync();
        }
    }
}
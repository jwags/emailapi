using EmailApi.Business.Helpers;
using EmailApi.Business.Models;
using EmailApi.Entities;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;

namespace EmailApi.Business.BusinessLogic
{
    public enum EmailResultCode
    {
        None = 0,
        Okay = 1,
        NullItemInput = -2,
        Error = -3,
        DataValidationError = 4
    }

    public interface IEmailBusinessLogic
    {
        Task<BusinessResult<EmailResultCode, Email>> SendEmail(EmailCommandModel email);
    }

    public class EmailBusinessLogic : IEmailBusinessLogic
    {
        public static readonly IDictionary<EmailResultCode, string> ErrorMessages = new Dictionary<EmailResultCode, string>
                                                                                         {
                                                                                             { EmailResultCode.None, string.Empty },
                                                                                             { EmailResultCode.Okay, string.Empty },
                                                                                             { EmailResultCode.Error, "Unexpected error." },
                                                                                             { EmailResultCode.NullItemInput, "No value was provided." },
                                                                                         };

        public async Task<BusinessResult<EmailResultCode, Email>> SendEmail(EmailCommandModel email)
        {
            if (email == null)
            {
                return new BusinessResult<EmailResultCode, Email>(false, EmailResultCode.NullItemInput, EmailResultCode.NullItemInput.ToString(), null);
            }

            var modelValidationResult = DataAnnotationsHelper.ValidateModel(email);
            if (modelValidationResult.IsFailure())
            {
                return new BusinessResult<EmailResultCode, Email>(false, EmailResultCode.DataValidationError, null, null);
            }

            try
            {
                // TODO
                // Save request to db

                // Send Email
                MailMessage mail = new MailMessage();
                mail.To.Add(email.Destination);
                mail.From = new MailAddress(email.Email);
                mail.Subject = email.Subject;
                mail.Body = email.Message;
                mail.IsBodyHtml = true;
                var smtp = new SmtpClient();
                smtp.UseDefaultCredentials = true;
                smtp.Host = "localhost";
                await smtp.SendMailAsync(mail);

                return new BusinessResult<EmailResultCode, Email>(true, EmailResultCode.Okay, string.Empty, null);
            }
            catch (Exception e)
            {
                // TODO
                // Log exception
                Console.WriteLine($"Error sending email: {e.ToString()}");
                return new BusinessResult<EmailResultCode, Email>(false, EmailResultCode.Error, e.ToString(), null);
            }
        }
    }
}
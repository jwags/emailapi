namespace EmailApi.Business.Models
{
    public interface IBusinessResult<TOne, TTwo>
    {
        bool IsSuccess { get; set; }
        bool IsFailure();
    }

    public class BusinessResult<TOne, TTwo> : IBusinessResult<TOne, TTwo>
    {
        public BusinessResult(bool isSuccess, TOne resultCode, string errorMessage, TTwo entity)
        {
            IsSuccess = isSuccess;
            ResultCode = resultCode;
            ErrorMessage = errorMessage;
            Entity = entity;
        }

        public bool IsSuccess { get; set; }
        TOne ResultCode { get; set; }
        public string ErrorMessage { get; set; }
        TTwo Entity { get; set; }

        public bool IsFailure() => this.IsSuccess == false;
    }
}
using System.ComponentModel.DataAnnotations;

namespace EmailApi.Business.Models
{
    public class EmailCommandModel
    {
        [Required]
        [MaxLength(64)]
        public string Name { get; set; }

        [Required]
        [MaxLength(64)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [MaxLength(64)]
        [DataType(DataType.EmailAddress)]
        public string Destination { get; set; }

        [Required]
        [MaxLength(200)]
        public string Subject { get; set; }

        [Required]
        [MaxLength(4000)]
        public string Message { get; set; }
    }
}
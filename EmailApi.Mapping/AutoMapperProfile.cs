﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using EmailApi.Mapping.Profiles;

namespace EmailApi.Mapping
{
    public static class AutoMapperProfile
    {
        public static void Build(IServiceCollection services)
        {
            services
                .AddAutoMapper(typeof(EmailProfile));
        }
    }
}

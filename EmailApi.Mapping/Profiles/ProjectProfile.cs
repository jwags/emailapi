using AutoMapper;
using EmailApi.Entities;
using EmailApi.Models;

namespace EmailApi.Mapping.Profiles
{
    public class EmailProfile : Profile
    {
        public EmailProfile()
        {
            this.CreateMap<Email, EmailDto>();
        }
    }
}
using Microsoft.EntityFrameworkCore;
using EmailApi.Entities;

namespace EmailApi.ContextConfiguration
{
    public class EmailContext : DbContext
    {
        public EmailContext(DbContextOptions<EmailContext> options)
            : base(options)
        {
        }

        public DbSet<Email> Emails { get; set; }
    }
}
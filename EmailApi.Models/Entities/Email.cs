using System.ComponentModel.DataAnnotations;

namespace EmailApi.Entities
{
    public class Email
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(64)]
        public string Name { get; set; }

        [Required]
        [MaxLength(64)]
        [DataType(DataType.EmailAddress)]
        public string ResponseEmail { get; set; }

        [Required]
        [MaxLength(64)]
        [DataType(DataType.EmailAddress)]
        public string Destination { get; set; }

        [Required]
        [MaxLength(4000)]
        public string Message { get; set; }

        public bool IsVoid { get; set; }
    }
}
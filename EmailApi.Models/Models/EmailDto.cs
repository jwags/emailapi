namespace EmailApi.Models
{
    public class EmailDto
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Destination { get; set; }
        public string Message { get; set; }
    }
}